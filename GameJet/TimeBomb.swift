//
//  TimeBomb.swift
//  GameJet
//
//  Created by Li Wang on 30/07/2014.
//  Copyright (c) 2014 Li Wang. All rights reserved.
//

import Foundation

import SpriteKit


class TimeBomb : SpriteNodeContainer {

    var hurt : Int32 = 200
    
    init(){
        super.init(node: SKSpriteNode(imageNamed:"timebomb"))
        self.node.setScale(0.2)
    }
    
    

}