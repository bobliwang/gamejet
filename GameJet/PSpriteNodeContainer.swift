//
//  PSpriteNodeContainer.swift
//  GameJet
//
//  Created by Li Wang on 29/07/2014.
//  Copyright (c) 2014 Li Wang. All rights reserved.
//

import Foundation

import SpriteKit


protocol PSpriteNodeContainer {
    var node : SKSpriteNode {
    get
    set
    }
}



extension SKSpriteNode {
    
    func checkOverlapping(anotherNode : SKSpriteNode) -> Bool {
        
        var dx = abs(self.position.x - anotherNode.position.x)
        var dy = abs(self.position.y - anotherNode.position.y)
        
        if dx <= (self.size.width + anotherNode.size.width)/2 {
            if dy <= (self.size.height + anotherNode.size.height)/2{
                return true
            }
        }
        
        return false
    }
}