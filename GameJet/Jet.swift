//
//  Jet.swift
//  GameJet
//
//  Created by Li Wang on 29/07/2014.
//  Copyright (c) 2014 Li Wang. All rights reserved.
//

import Foundation
import SpriteKit

class Jet : SpriteNodeContainer {
    
    var jetSpeed : CGFloat = 150
    
    var rockets : [Rocket]
    var launchedRockets : [Rocket]
    var hpHint = SKSpriteNode(imageNamed: "hp")
    var hp : Int32 = 100
    
//    var rocketNodeHint1 = SKSpriteNode(imageNamed: "rocket")
//    var rocketNodeHint2 = SKSpriteNode(imageNamed: "rocket")
    
    init(){
        
        self.rockets = [Rocket]()
        self.launchedRockets = [Rocket]()
        
        for var i = 0; i < 3; i++ {
            self.rockets.append(Rocket())
        }
        
        super.init(node: SKSpriteNode(imageNamed:"Spaceship"))
        
        self.node.xScale = 0.15
        self.node.yScale = 0.15
        
        
        hpHint.xScale = 0.8
        hpHint.yScale = 0.1
        
        hpHint.position = CGPoint(x:CGRectGetMidX(self.node.frame), y:CGRectGetMinY(self.node.frame) - self.node.size.height*4)
        
        self.node.addChild(hpHint)
        
//        rocketNodeHint1.position = CGPoint(x:CGRectGetMidX(self.node.frame)-200, y:CGRectGetMidY(self.node.frame)-100)
//        rocketNodeHint2.position = CGPoint(x:CGRectGetMidX(self.node.frame)+200, y:CGRectGetMidY(self.node.frame)-100)
//        rocketNodeHint1.xScale = 0.3
//        rocketNodeHint1.yScale = 0.3
//        rocketNodeHint2.xScale = 0.3
//        rocketNodeHint2.yScale = 0.3
//        self.node.addChild(rocketNodeHint1)
//        self.node.addChild(rocketNodeHint2)
    }
    
    override func destroy() {
//        for r in [rocketNodeHint1, rocketNodeHint2]{
//            r.texture = SKTexture(imageNamed: "explosion\(rand()%5)")
//            
//            r.runAction(SKAction.scaleTo(0.8, duration: 0.5), {
//                
//                    r.runAction(SKAction.scaleTo(0.05, duration: 1))
//            })
//        }
        
        super.destroy()
    }
    
    func changeHp(delta : Int32){
        self.hp += delta
        self.hpHint.xScale = CGFloat(Double(self.hp > 0 ? self.hp : 0)/100.0) * 0.8

    }
    
    func launchRocket(){
        
        if self.rockets.count > 0{
            let rocket = self.rockets.removeLast()
            self.launchedRockets.append(rocket)
            
            rocket.position = CGPoint(x:self.position.x, y:self.position.y + rocket.node.size.height)
            rocket.addToScene(node.scene)
            rocket.startMovingFromJet(self)
        }
        
    }
    
    func rocketDestroyed(rocket : Rocket){
        self.rockets.append(Rocket())
        
        self.launchedRockets = self.launchedRockets.filter( {$0.uuid != rocket.uuid })
        
        rocket.destroy()
    }
    
}