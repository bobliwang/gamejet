//
//  SpriteNodeContainer.swift
//  GameJet
//
//  Created by Li Wang on 29/07/2014.
//  Copyright (c) 2014 Li Wang. All rights reserved.
//

import Foundation
import SpriteKit



class SpriteNodeContainer : PSpriteNodeContainer {
    var node : SKSpriteNode
    
    var position : CGPoint {
    get{
        return self.node.position
    }
    set(value){
        self.node.position = value
    }
    }
    
    var size : CGSize {
    get{
        return self.node.size
    }
    }
    
    init(node : SKSpriteNode){
        self.node = node
    }
    
    func destroy(){
        
        var idx = random()%5
        
        var fileName = "explosion\(idx)"
        
        self.node.texture = SKTexture(imageNamed: fileName)
        self.node.xScale = 0.3
        self.node.yScale = 0.3
        self.node.removeAllActions()
        
        self.node.runAction(SKAction.scaleTo(0.08, duration: 1), {
            self.node.removeAllActions()
            self.node.removeAllChildren()
            self.removeFromParent()
        })
        
    }
    
    func destroy(action : ()->Void){
        var idx = random()%5
        
        var fileName = "explosion\(idx)"
        
        self.node.texture = SKTexture(imageNamed: fileName)
        self.node.xScale = 0.3
        self.node.yScale = 0.3
        self.node.removeAllActions()
        
        self.node.runAction(SKAction.scaleTo(0.08, duration: 1), {
            self.node.removeAllActions()
            self.node.removeAllChildren()
            self.removeFromParent()
            
            action()
        })

    }
    
    func removeFromParent(){
        self.node.removeFromParent()
    }
    
    func addToScene(scene : SKScene){
        scene.addChild(self.node)
    }
}