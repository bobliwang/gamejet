//
//  GameScene.swift
//  GameJet
//
//  Created by Li Wang on 29/07/2014.
//  Copyright (c) 2014 Li Wang. All rights reserved.
//

import SpriteKit

class GameScene: SKScene {
    
    var jet = Jet()
    
    var totalKill  = 0
    
    var backgroundImage = SKSpriteNode(imageNamed: "universe")
    var backgroundImage1 = SKSpriteNode(imageNamed: "universe")
    
    var monsters = [Monster]()
    
    var infoBoard = SKLabelNode(text: "")
    
    func resetBackground(){
        for bg in [backgroundImage, backgroundImage1]{
            bg.setScale(2.8)
        }

        backgroundImage.position = CGPoint(x:CGRectGetMidX(self.view.frame), y:CGRectGetMidY(self.view.frame))
        
        backgroundImage1.position = CGPoint(x:CGRectGetMidX(self.view.frame), y:CGRectGetMidY(self.view.frame) + self.view.frame.height)
        
        var movingDown = SKAction.repeatActionForever(SKAction.moveBy(CGVector(0,-80), duration: 1))
        
        backgroundImage.runAction(movingDown)
        backgroundImage1.runAction(movingDown)

    }
    
    override func didMoveToView(view: SKView) {
        resetBackground()
        
        self.addChild(backgroundImage)
        self.addChild(backgroundImage1)
        
        infoBoard.position = CGPointMake(CGRectGetMidX(self.frame), CGRectGetMaxY(self.frame) - 50)
        self.addChild(infoBoard)
        
        addSpaceship()
    }
    
    override func touchesBegan(touches: NSSet, withEvent event: UIEvent) {
        /* Called when a touch begins */
        
        
        for touch: AnyObject in touches {
            var location = touch.locationInNode(self)
                        
            var distance = location.distanceFrom(jet.node.position)
            var time = Double(distance / jet.jetSpeed)
            
            var dx = CGFloat(jet.node.position.x - location.x)
            var angle = CGFloat(M_PI_4)*(dx)/jet.node.position.distanceFrom(location)
            
            
            var rotAction = SKAction.rotateToAngle(angle, duration: 0.00)
            let move = SKAction.moveTo(location, duration: time);
            
            jet.node.removeAllActions()
            
            self.jet.node.runAction(rotAction, {
                self.jet.node.runAction(move, {
                    self.jet.node.runAction(SKAction.rotateToAngle(0, duration: 0.00))
                    })
                })
            
            jet.launchRocket()
        }
    }
    
    func addSpaceship(){
        
        jet.node.position = CGPoint(x:CGRectGetMidX(self.frame),y:CGRectGetMinY(self.frame) + jet.node.size.height)
        self.addChild(jet.node)
    }
    
    var bombs = [TimeBomb]()
    
    func addTimeBomb() {
        var bomb = TimeBomb()
        
        bomb.node.position = CGPoint(x:CGRectGetMidX(self.frame),y:CGRectGetMaxY(self.frame))
        bomb.node.runAction(SKAction.repeatActionForever(SKAction.moveBy(CGVector(0,-100), duration: 1)))
        
        bomb.node.runAction(SKAction.repeatActionForever(SKAction.rotateByAngle(1, duration: 1)))
        
        self.bombs.append(bomb)
        self.addChild(bomb.node)
    }
    
    func addMonster(){
        
        var monster = Monster()
        
        var xPos = CGRectGetMidX(self.frame);
        var dir1 = CGFloat((rand()%3)-1)
        xPos += (dir1 * (self.view.frame.width/4))
        
        monster.startMoveInScene(self)
        
        monster.position = CGPoint(x:xPos, y:CGRectGetMaxY(self.frame) - monster.node.size.height/2)
        
        monsters.append(monster)
    }
    
    func adjustBackground(){
        
        if backgroundImage.position.y + backgroundImage.size.height/2 < 0 {
            backgroundImage.position.y = backgroundImage1.position.y + backgroundImage.size.height
        }
        
        if backgroundImage1.position.y + backgroundImage1.size.height/2 < 0 {
            backgroundImage1.position.y = backgroundImage.position.y + backgroundImage1.size.height
        }
    }
    
    func processMonstersOutofScope(){
        for var i = self.monsters.count-1; i >= 0; i-- {
            var monster = self.monsters[i]
            var node = monster.node
            
            if node.position.y > CGRectGetMaxY(self.view.frame) || node.position.y < CGRectGetMinY(self.view.frame)
                || node.position.x > CGRectGetMaxX(self.view.frame) || node.position.x < CGRectGetMinX(self.view.frame)
                
            {
                monster.removeFromParent()
                self.monsters.removeAtIndex(i)
                self.addMonster()
            }
        }
        
        
        for var i = self.bombs.count-1; i >= 0; i-- {
            var bomb = self.bombs[i]
            var node = bomb.node
            
            if node.position.y > CGRectGetMaxY(self.view.frame) || node.position.y < CGRectGetMinY(self.view.frame)
                || node.position.x > CGRectGetMaxX(self.view.frame) || node.position.x < CGRectGetMinX(self.view.frame)
                
            {
                bomb.removeFromParent()
                self.bombs.removeAtIndex(i)
            }
        }
    }
    
    func checkJetBeingHit(){
        for var i = self.monsters.count - 1; i >= 0; i-- {
            var monster = self.monsters[i]
            
            if monster.node.checkOverlapping(self.jet.node) {
                monster.destroy()
                
                self.jet.changeHp(-1*monster.hurt)
                    
                self.monsters.removeAtIndex(i)
                self.addMonster()
                
                if self.jet.hp <= 0 {
                    
                    self.jet.destroy()
                    self.jet = Jet()
                    self.addSpaceship()
                }
                
            }
            
        }
    }
    
    func checkBombs(){
        for var j = self.bombs.count - 1; j >= 0; j-- {
            var b = self.bombs[j]
            
            if b.node.checkOverlapping(jet.node) {
                for var i = self.monsters.count-1; i >= 0; i-- {
                    
                    var monster = self.monsters[i]
                    
                    monster.changeHp(-1*b.hurt)
                    
                    if(monster.hp <= 0){
                        monster.destroy()
                        self.monsters.removeAtIndex(i)
                        
                        self.totalKill += 1
                        
                    }
                }
                
                b.destroy()
                
                self.bombs.removeAtIndex(j)
            }
            
        }
    }
    
    func checkRocketHits(){
        for var j = self.jet.launchedRockets.count - 1; j >= 0; j-- {
            var r = self.jet.launchedRockets[j]
            
            var hit = false
            
            for var i = self.monsters.count-1; i >= 0; i-- {
                
                var monster = self.monsters[i]
                
                if r.canHitAnotherNode(monster.node){
                    hit = true
                    
                    println("hit **************")
                    
                    monster.changeHp(-1*r.hurt)
                    
                    if(monster.hp <= 0){
                        monster.destroy()
                        self.monsters.removeAtIndex(i)
                        
                        self.totalKill += 1

                    }
                }
            }
            
            
            if hit {
                r.destroy()
                self.jet.rockets.append(Rocket())
                self.jet.launchedRockets.removeAtIndex(j)
            }
            
        }

    }
    
    func updateInfoBoard(){
        infoBoard.text = "HP : \(self.jet.hp), Your kills: \(self.totalKill), Level: \(getCurrentLevel())"
        infoBoard.fontColor = UIColor.yellowColor()
    }
    
    func getCurrentLevel() -> Int {
        var temp = ((self.totalKill)/5 + 1)
        
        if temp > 5 {
            temp = 5
        }
        
        return temp
    }
    
    
    override func update(currentTime: CFTimeInterval) {
        self.jet.node.setScale(0.15)
        if self.monsters.count < self.getCurrentLevel() * 4 {
            self.addMonster()
        }
        
        self.adjustBackground()
        
        self.processMonstersOutofScope()
        
        self.checkRocketHits()
        
        self.checkJetBeingHit()
        self.updateInfoBoard()
        
        if rand() % 500 == 0 {
            
            self.addTimeBomb()
        }
        
        self.checkBombs()
        
    }
}

extension CGPoint {
    func distanceFrom(point : CGPoint) -> CGFloat {
        var dx = (self.x - point.x)
        var dy = (self.y - point.y)
        
        var dist = pow(pow(dx, 2) + pow(dy, 2), 0.5)
        
        
        return dist
    }
}

