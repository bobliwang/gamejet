//
//  Rocket.swift
//  GameJet
//
//  Created by Li Wang on 29/07/2014.
//  Copyright (c) 2014 Li Wang. All rights reserved.
//

import Foundation
import SpriteKit


class Rocket : SpriteNodeContainer {
    
    var rocketSpeed : CGFloat = 550
    
    var hurt : Int32 = 100
    
    
    var uuid = NSUUID()
    
    init(){
        
        super.init(node: SKSpriteNode(imageNamed:"rocket"))
        
        self.node.xScale = 0.1
        self.node.yScale = 0.1
    }
    
    func startMovingFromJet(jet : Jet) {
        let missileFly = SKAction.moveBy(CGVector(0, self.rocketSpeed), duration: 1)
        
        self.node.runAction(missileFly, completion: {
            jet.rocketDestroyed(self)
        })
    }
    
    func canHitAnotherNode(anotherNode : SKSpriteNode) -> Bool {
        
        var dx = abs(self.position.x - anotherNode.position.x)
        var dy = abs(self.position.y - anotherNode.position.y)
        
        if dx <= (self.size.width + anotherNode.size.width)*1/2 {
            if dy <= (self.size.height + anotherNode.size.height)*1/2{
                return true
            }
        }
        
        return false
    }
    
}
