//
//  Monster.swift
//  GameJet
//
//  Created by Li Wang on 29/07/2014.
//  Copyright (c) 2014 Li Wang. All rights reserved.
//

import Foundation
import SpriteKit



class Monster : SpriteNodeContainer {
    
    var monsterSpeed : CGFloat = 150
    
    var monsterType : Int32
    var hurt : Int32 = 80 + rand()%10
    
    var hp : Int32 = 100
    
    var hpHint = SKSpriteNode(imageNamed: "hp")
    
    init(monsterType : Int32){
        self.monsterSpeed = CGFloat(Double(monsterType + 1)*30.0)
        self.monsterType = monsterType
        super.init(node: SKSpriteNode(imageNamed:"monster\(self.monsterType)"))
        
        var rate = (Double(hp)/100.0)
        
        self.node.setScale(0.25*rate.bridgeToObjectiveC())
                
        hpHint.position = CGPoint(x:CGRectGetMidX(self.node.frame), y:CGRectGetMaxY(self.node.frame) + hpHint.size.height)
        hpHint.xScale = 0.3
        hpHint.yScale = 0.1
        self.node.addChild(hpHint)
    }
    
    
    
    convenience init(){
        self.init(monsterType: rand()%4)
    }
    
    func changeHp(delta : Int32){
        self.hp += delta
        self.hpHint.xScale = CGFloat(Double(self.hp > 0 ? self.hp : 0)/100.0) * 0.3
        
        println("------------ HP: \(self.hpHint.xScale)" )
    }
    
    func startMoveInScene(scene : SKScene){
        
        var dir = rand()%3 - 1
        
        var xSpeed = Double( dir*40 + rand()%10)
        
        var action = SKAction.moveBy(CGVector(CGFloat(xSpeed), -monsterSpeed), duration: 1)
        
        self.addToScene(scene)
        
        
        var monsterMove = SKAction.repeatActionForever(action)
        self.node.runAction(monsterMove)
        
    }
}
